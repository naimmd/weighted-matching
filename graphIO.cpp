    /*

    Copyright (C) 2016 Md. Naim

    This file is part of Mesite - CUDA C++ parallel program to compute 1-matching. 

    Mesite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mesite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mesite.  If not, see <http://www.gnu.org/licenses/>.
    
    */

#include <iostream>
//#include <string>
#include "string.h"
#include <fstream>
#include <stdlib.h>

#include "graphdef.h"
#include "graphIO.h"
#include "graphMemory.h"

char* writeGraphInBinary(const char* f_name, Graph* graph) {

    std::string fileName(f_name);

    fileName.append(std::to_string(graph->nrVertices)).append("_");
    fileName.append(std::to_string(graph->nrEdges)).append("_");
    fileName.append(std::to_string(graph->graphType)).append(".bin");

    std::cout << "Writing to " << fileName << std::endl;

    std::ofstream ofs;
    ofs.open(fileName.c_str(), std::ios::out | std::ios::binary);

    if (graph) {
        ofs.write((char*) &graph->nrVertices, sizeof (uint));

        ofs.write((char*) &graph->nrEdges, sizeof (uint));

        //Write the type of graph along with #V and #E
        ofs.write((char*) &graph->graphType, sizeof (uint)); // -----<<<


        if (graph->indices && graph->edges) {

            ofs.write((char*) &graph->indices[0], sizeof (uint)*(graph->nrVertices + 1));

            ofs.write((char*) &graph->edges[0], sizeof (uint)*(graph->nrEdges));

        } else {
            printf("\nIn indices and edges  can't be NULL\n");
            exit(EXIT_FAILURE);
        }


        if (graph->graphType == WEIGHTED) {

            if (graph->weights) {

                ofs.write((char*) &graph->weights[0], sizeof (EdgeType)*(graph->nrEdges));

            } else {
                printf("\nIn Weighted Graph weights can't be NULL\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    ofs.close();

    char* nameToReturn = (char*) malloc(sizeof (char)*(fileName.size() + 1));
    strcpy(nameToReturn, fileName.c_str());

    printf("Wrote to %s\n", fileName.c_str());
    return nameToReturn;
}

Graph* readGraphInBinary(const char* fileName) {


    Graph* graph = NULL;

    std::ifstream ifs;
    ifs.open(fileName, std::ios::in | std::ios::binary);
    if (ifs.is_open()) {

        uint nrVertices;
        uint nrEdges;
        uint graphType;

        ifs.read((char*) &nrVertices, sizeof (uint));
        ifs.read((char*) &nrEdges, sizeof (uint));
        ifs.read((char*) &graphType, sizeof (uint));

        //Allocate memory for the graph
        graph = allocateGraph(nrVertices, nrEdges, graphType);

        if (graphType == WEIGHTED)
            printf("\nReading a weighted graph\n");
        else
            printf("\nReading a un-weighted graph\n");


        printf("\nReading File with %u vertices, %u edges and %u weights\n",
                graph->nrVertices, graph->nrEdges, graph->graphType * graph->nrEdges);


        if (graph->indices && graph->edges) {

            ifs.read((char*) &graph->indices[0], sizeof (uint)*(graph->nrVertices + 1));

            ifs.read((char*) &graph->edges[0], sizeof (uint)*(graph->nrEdges));

        } else {
            printf("ERROR: One of the array is not allocated before reading \n");
        }

        //Only for weighted graph
        if (graph->graphType == WEIGHTED) {
            if (graph->weights) {
                ifs.read((char*) &graph->weights[0], sizeof (EdgeType) * graph->nrEdges);
            } else {
                printf("ERROR: graph->weights needs to allocated before reading \n");
            }
        }

    } else {
        printf("\nERROR:Can't open inputGraph\n");
    }
    return graph;
}

