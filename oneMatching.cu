/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <stdbool.h>

#include "crossDevConstants.h"
#include "matchingKernelsDef.h"
#include "stdio.h"

#ifdef RUNONGPU

__device__
#endif
    void
    printGraphFromGPU(Graph *graph, uint *endPointsOfEdges,
                      uint *suitorIndices) {

  for (uint i = 0; i < min(graph->nrVertices, 18); i++) {
    printf("{v = %u: %u : |N(v)| %u}", i, suitorIndices[i],
           (graph->indices[i + 1] - graph->indices[i]));
    for (uint j = graph->indices[i]; j < graph->indices[i + 1]; j++) {
      printf("[%u %.2f]  ", graph->edges[j],
             graph->weights[j] /*, endPointsOfEdges[j]*/);
    }
    printf("\n");
  }
}

#ifdef RUNONGPU

__device__
#endif
    uint
    findCandidate(uint vId, const Graph *graph, uint *endPointsOfEdges,
                  uint *suitorIndices) {
  // determine #neighbors
  uint startOfNbr = graph->indices[vId];
  uint endOfNbr = graph->indices[vId + 1];

  uint bestNbr = graph->nrVertices;
  EdgeType bestWgt = -1.0;
  uint bestIdx = graph->nrEdges;
  uint prevSuitorIdx = graph->nrEdges;

  // NOTE; ID 0 of original warp (according to WARPSIZE); not modified
  // one(according to wrpSz)

  uint laneId = threadIdx.x & (WARPSIZE - 1); // NEVER USE wrpSz here!
  uint returnVtx = graph->nrVertices;

  for (int idx = startOfNbr + laneId; idx < endOfNbr; idx = idx + WARPSIZE) {
    uint y = graph->edges[idx];
    EdgeType wgt2y = (graph->weights == NULL) ? 1.0 : graph->weights[idx];

    // printf("y=%u, wgt2y=%f\n", y, wgt2y);

    // compare against best found so far
    if (wgt2y < bestWgt)
      continue;
    if (wgt2y == bestWgt && y < bestNbr)
      continue; // prefer higherId

    // compare against current offer of neighbor
    uint partnerIdx = suitorIndices[y];

    if (partnerIdx < graph->nrEdges) {

      uint currPartner =
          endPointsOfEdges[partnerIdx]; // graph->edges[partnerIdx]; //NOTE IT
      EdgeType wgt2partner =
          (graph->weights == NULL) ? 1.0 : graph->weights[partnerIdx];

      if (wgt2y < wgt2partner)
        continue;
      if (wgt2y == wgt2partner && vId < currPartner)
        continue;
    }

    // If the thread reach here ; that means current neighbor can be offered
    bestIdx = idx;
    bestNbr = y;
    bestWgt = wgt2y;
    prevSuitorIdx = partnerIdx;
  }

  // Determine size of warp according to nrNbr to save time in reduction phase

  uint wrpSz = WARPSIZE;

  uint nrNbr = endOfNbr - startOfNbr;

  // if (nrNbr <= wrpSz>>4) wrpSz = wrpSz>>4;
  if (nrNbr <= wrpSz >> 3)
    wrpSz = wrpSz >> 3;
  else if (nrNbr <= wrpSz >> 2)
    wrpSz = wrpSz >> 2;
  else if (nrNbr <= wrpSz >> 1)
    wrpSz = wrpSz >> 1;

  // NOTE: Reduction with local wrpSz instead of global WARPSIZE

  for (int i = wrpSz >> 1; i >= 1; i = i >> 1) {

    EdgeType recWgt = __shfl_xor_sync(bestWgt, i, wrpSz); // float
    uint recVtx = __shfl_xor_sync(bestNbr, i, wrpSz);     // integer
    uint recvIdx = __shfl_xor_sync(bestIdx, i, wrpSz);    // integer
    uint recvPrevSuitorIdx =
        __shfl_xor_sync(prevSuitorIdx, i, wrpSz); // integer

    if (recWgt > bestWgt) {
      bestNbr = recVtx;
      bestWgt = recWgt;
      bestIdx = recvIdx;
      prevSuitorIdx = recvPrevSuitorIdx;
    } else if (recWgt == bestWgt) { // give priority to higher indexed one
      if (recVtx > bestNbr) {
        bestNbr = recVtx;
        bestWgt = recWgt;
        bestIdx = recvIdx;
        prevSuitorIdx = recvPrevSuitorIdx;
      }
    }
  }

  if (laneId == 0) {
    if (bestIdx < graph->nrEdges) { // Found a neighbor to offer

      uint currSuitorIdx =
          atomicCAS(&suitorIndices[bestNbr], prevSuitorIdx, bestIdx);

      if (currSuitorIdx == prevSuitorIdx) { // successfully offered

        if (prevSuitorIdx < graph->nrEdges) { // dislodged a vertex

          returnVtx = endPointsOfEdges[prevSuitorIdx]; // Id of previous suitor
        } else {                                       // didn't dislodge anyone
          returnVtx = graph->nrVertices;               // means, nothing to do
        }
      } else { // couldn't  offer; need to find new partner for vId
        returnVtx = vId;
      }
    } else { // Didn't find a neighbor to offer; will remain unmatched
      returnVtx = graph->nrVertices; // means, nothing to do
    }
  }

  // if(laneId == 0 && (vId == 1493 || vId == 4595 ) )printf("\nvId = %u :
  // bestNbr = %u, returnVtx = %u\n\n", vId, bestNbr, returnVtx);
  returnVtx = __shfl_sync(
      returnVtx, 0,
      WARPSIZE); // NOTE: ID 0 of original warp (according to WARPSIZE)
  return returnVtx;
}

#ifdef RUNONGPU

__device__
#endif
    void
    setCandidate() {
}

#ifdef RUNONGPU

__global__
#endif
    void
    kernel_OneMatching(Graph devGraph, uint *endPointsOfEdges,
                       uint *suitorIndices, uint *devCounters,
                       uint targetStep) {

  // Global thread Id
  // uint threadId = blockIdx.x * blockDim.x + threadIdx.x;
  // local warp Id
  uint warpId = threadIdx.x / WARPSIZE;

  uint laneId = threadIdx.x & (WARPSIZE - 1);

  // Need to use local warpId
  extern __shared__ uint blockmem[];
  volatile uint *warpMem =
      blockmem + CHUNKSIZE * warpId; // each warp get memory of size (CHUNKSIZE)

  // global warp id
  warpId = blockIdx.x * (blockDim.x / WARPSIZE) + warpId;

  // count remaining vertices
  int nrVts2process = devGraph.nrVertices - warpId * CHUNKSIZE;

  if (nrVts2process >= CHUNKSIZE)
    nrVts2process = CHUNKSIZE;

  // if the warp got to do something
  if (nrVts2process > 0) {
    /*
    if (nrVts2process < CHUNKSIZE) {
        if (!laneId)printf("\nthreadId = %u, warpId = %u, #vts2process=%d\n",
    threadId, warpId, nrVts2process);
    }*/
    bool isFirstRound = true;
    uint stepId = 0;
    do {
      int wrpPtr = 0;
      uint vId;

      // if (warpId == DWID && !laneId)printf("\nBreaking the loop at step"
      //         " %u while #vtx2process =%d\n", stepId, nrVts2process);
      for (int offset = 0; offset < nrVts2process; offset++) {

        if (isFirstRound == true) {
          vId = warpId * CHUNKSIZE + offset;
        } else {
          vId = warpMem[offset];
        }
        uint returnVtx =
            findCandidate(vId, &devGraph, endPointsOfEdges, suitorIndices);

        if (returnVtx < devGraph.nrVertices) {
          if (!laneId)
            warpMem[wrpPtr] = returnVtx;
          wrpPtr++;
        }
      }

      if (!laneId)
        devCounters[warpId] = wrpPtr;

      nrVts2process = wrpPtr;
      isFirstRound = false;

      // if (stepId == targetStep) break;
      stepId++;

    } while (nrVts2process > 0);
  }
}

#ifdef RUNONGPU

__global__
#endif
    void
    kernel_Dump(Graph devGraph, uint *endPointsOfEdges, uint *suitorIndices,
                uint *devCounters, uint targetStep) {

  // Global thread Id
  uint threadId = blockIdx.x * blockDim.x + threadIdx.x;

  if (!threadId)
    printGraphFromGPU(&devGraph, endPointsOfEdges, suitorIndices);
}
