/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <stdio.h>

#include "assert.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "graphdef.h"

void allocateDevMem(void **memPtr, size_t nrBytes) {

  assert(cudaSuccess == cudaMalloc(memPtr, nrBytes));
}

void freeDevMem(void *memPtr) {
  if (memPtr != NULL)
    assert(cudaSuccess == cudaFree(memPtr));
}

/**
  Caller Module needs to verify the sizes of corresponding array
 */
void copyArray(void *dest, const void *source, size_t nrBytes,
               uint cpyDirection) {

  cudaMemcpyKind direction = (cudaMemcpyKind)cpyDirection;
  assert(dest != NULL && source != NULL && nrBytes > 0);

  assert(cudaSuccess == cudaMemcpy(dest, source, nrBytes, direction));
}

Graph *allocateGraphOnDev(const Graph *graph) {

  Graph *devGraph = NULL;
  if (graph) {

    // Main pointer on the Host;
    // Only member pointers are allocated on device

    devGraph = (Graph *)malloc(sizeof(Graph));

    if (graph->indices != NULL && graph->nrVertices > 0) {

      devGraph->nrVertices = graph->nrVertices;

      allocateDevMem((void **)&devGraph->indices,
                     (graph->nrVertices + 1) * sizeof(uint));

    } else {
      printf("\nERROR: indices not allocated on device; check corresponding "
             "host pointers\n");
    }

    if (graph->edges != NULL && graph->nrEdges > 0) {

      devGraph->nrEdges = graph->nrEdges;

      size_t nrBytes2Allocate = graph->nrEdges * sizeof(uint);

      allocateDevMem((void **)&devGraph->edges, nrBytes2Allocate);

    } else {
      printf("\nedges or weights or edges not allocated on device\n");
    }

    devGraph->graphType = graph->graphType;

    // If the graph is weighted then copy weights

    if (graph->graphType == WEIGHTED) {

      if (graph->weights != NULL) {

        size_t nrBytes2Allocate = graph->nrEdges * sizeof(EdgeType);

        allocateDevMem((void **)&devGraph->weights, nrBytes2Allocate);

      } else {
        printf("\nERROR:In Weighted Graph, host weights can't be NULL\n");
      }
    } else {
      devGraph->weights = NULL;
    }
  }
  return devGraph;
}

void copyGraphH2D(const Graph *hostGraph, Graph *devGraph) {

  if (hostGraph != NULL && devGraph != NULL) {

    // Make sure pointers are not NULL
    assert(hostGraph->indices != NULL && devGraph->indices != NULL &&
           devGraph->nrVertices >= hostGraph->nrVertices &&
           hostGraph->nrVertices > 0);

    // Copy Indices
    assert(cudaSuccess == cudaMemcpy(devGraph->indices, hostGraph->indices,
                                     (hostGraph->nrVertices + 1) * sizeof(uint),
                                     cudaMemcpyHostToDevice));

    assert(devGraph->nrEdges >= hostGraph->nrEdges && hostGraph->nrEdges > 0);

    size_t nrBytes2Cpy = hostGraph->nrEdges * sizeof(uint);

    // Copy Edges
    assert(cudaSuccess == cudaMemcpy(devGraph->edges, hostGraph->edges,
                                     nrBytes2Cpy, cudaMemcpyHostToDevice));

    if (hostGraph->graphType == WEIGHTED) {

      nrBytes2Cpy = hostGraph->nrEdges * sizeof(EdgeType);

      assert(cudaSuccess == cudaMemcpy(devGraph->weights, hostGraph->weights,
                                       nrBytes2Cpy, cudaMemcpyHostToDevice));
    }

  } else {
    printf("\nBoth host and device graphs need to allocated beforehand\n");
  }
}

void freeDevGraph(Graph *devGraph) {

  if (devGraph) {
    freeDevMem(devGraph->indices);
    freeDevMem(devGraph->edges);
    if (devGraph->graphType == WEIGHTED)
      freeDevMem(devGraph->weights);
    free(devGraph);
    printf("\nFreed Graph\n");
  }
}
