/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "crossDevConstants.h"
#include "matchingKernelsDef.h"
#include "stdio.h"

/*
 *  Method to lock a memory location
 *
 */
__device__ int lock_vertex(volatile int *mutex, int id) {

  // volatile int* mutex = mutex_NV;
  if (atomicCAS((int *)(mutex + id), 0, 1) == 0)
    return 1;
  return 0;
}

/*
 * Method to Unlock a Memory Location
 */
__device__ void unlock_vertex(volatile int *mutex, int id) {
  // volatile int* mutex = mutex_NV;
  atomicExch((int *)(mutex + id), 0);
}

#ifdef RUNONGPU

__global__
#endif
    void
    kernelDecideEndPoints(Graph devGraph, uint *endPtsOfEdges) {

  // local warp Id
  uint warpId = threadIdx.x / WARPSIZE;

  // global warp id
  warpId = blockIdx.x * (blockDim.x / WARPSIZE) + warpId;

  // count remaining vertices
  int nrVts2process = devGraph.nrVertices - warpId * CHUNKSIZE;

  // if(!laneId)printf("\nwarpId = %u, Vts2Process = %u\n", warpId,
  // nrVts2process);

  if (nrVts2process >= CHUNKSIZE)
    nrVts2process = CHUNKSIZE;

  // if the warp got to do something
  if (nrVts2process > 0) {

    for (int offset = 0; offset < nrVts2process; offset++) {

      uint vId = warpId * CHUNKSIZE + offset;
      for (uint j = devGraph.indices[vId]; j < devGraph.indices[vId + 1]; j++) {
        endPtsOfEdges[j] = vId;
      }
    }
  }
}
