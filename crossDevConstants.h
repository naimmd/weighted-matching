/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CROSSDEVCONSTANTS_H
#define CROSSDEVCONSTANTS_H

#define WARPSIZE 16
#define CHUNKSIZE 16
#define NOSUITOR 0xffffffff
#define bValue 1

#endif /* CROSSDEVCONSTANTS_H */
