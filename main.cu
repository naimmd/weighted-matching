/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <algorithm>
#include <stdio.h>

#include "crossDevConstants.h"
#include "graphCheck.h"
#include "graphIO.h"
#include "graphMemory.h"
#include "graphdef.h"
#include "matchingDriver.h"
#include "stdlib.h"
using namespace std;

/*Makes a toy graph
 *
 */
Graph *makeGrap() {

  uint nrV = 5;
  uint nrE = 16;

  uint edges[] = {1, 2, 3, 0, 2, 4, 0, 1, 3, 4, 0, 2, 4, 1, 2, 3};
  uint ptrs[] = {0, 3, 6, 10, 13, 16};
  /*
    uint nrV = 3;
    uint nrE = 6;

    uint edges[] = {1, 2, 0, 2, 0, 1};
    uint ptrs[] = {0, 2, 4, 6};

    uint nrV = 2;
    uint nrE = 2;

    uint edges[] = {1, 0};
    uint ptrs[] = {0, 1, 2};

*/

  Graph *grah = allocateGraph(nrV, nrE, 1);
  std::copy(ptrs, ptrs + nrV + 1, grah->indices);

  std::copy(edges, edges + nrE, grah->edges);

  for (int i = 0; i < grah->nrEdges; i++) {
    grah->weights[i] = 1;
  }
  displayGraph(grah);
  return grah;
}

int main(int argc, char **argv) {

  if (argc < 2) {
    printf("Usage: %s [inputgraph]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const char *fileName = argv[1];

  // Read a graph in binary format

  Graph *graph = /* makeGrap(); */ readGraphInBinary(fileName);

  // displaySparse(graph);

  computeOneMatching(graph, bValue == 1);
  // computeOneMatching(graph, false);

  // Go Green!
  freeGraph(graph);

  if (graph == NULL)
    exit(EXIT_FAILURE);

  return 0;
}
