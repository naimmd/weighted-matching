    /*

    Copyright (C) 2016 Md. Naim

    This file is part of Mesite - CUDA C++ parallel program to compute 1-matching. 

    Mesite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mesite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mesite.  If not, see <http://www.gnu.org/licenses/>.
    
    */

#include "graphdef.h"
#include "graphCheck.h"
#include <vector>
#include <cmath>
#include <algorithm>
#include <stdio.h>
#include <numeric>

void getGraphStatistics(Graph* graph) {

    std::vector<uint> degs(graph->nrVertices + 1);
    std::adjacent_difference(graph->indices, graph->indices + graph->nrVertices + 1, degs.begin());

    uint maxDegree = 0;
    maxDegree = *std::max_element(degs.begin(), degs.end());

    uint nrBin = 31;
    std::vector<uint> bins(nrBin, 0);

    uint nrDegZero = 0;
    for (uint i = 1; i <= graph->nrVertices; i++) {

        if (degs[i] > 0) {

            int binId = (int) log2(degs[i]);
	    uint access = degs[i] - (1 << binId);
           // printf("\ndegree = %u, binId =%d\n", degs[i], binId);
            bins[binId + (access > 0) ]++;
            //bins[binId]++;

        } else if (degs[i] == 0) {

            //printf("\n*degree = %u\n", degs[i]);
            nrDegZero++;

        } else {
            printf("\nDegree can't be negative\n");
        }
    }

    printf("\n");
    for (int i = 0; i < nrBin; i++) {
        printf("%d %u\n", i, bins[i]);
    }
    printf("\n");


    /*
    std::string s = std::accumulate(v.begin(), v.end(), std::string {}, [](const std::string& a, int b) {
        return a.empty() ? std::to_string(b) : a + '-' + std::to_string(b);
    });
     */
}

void displaySparse(Graph* graph) {

    unsigned int i = graph->nrVertices - 1;

    printf("id=%u %u %u\n", i, graph->indices[i], graph->indices[i + 1]);

    for (int j = graph->indices[i]; j < graph->indices[i + 1]; j++) {

        int neighbor = graph->edges[j];
        EdgeType bond = graph->weights[j];

        printf("%d:%f ", neighbor, bond);
    }
    printf("\n");

}

void displayGraph(Graph* graph) {

    printf("\nFrom Man Side:\n");
    for (int i = 0; i < graph->nrVertices; i++) {
        printf("%d (%u) : ", i, (graph->indices[i+1]-graph->indices[i]));
        for (int j = graph->indices[i]; j < graph->indices[i + 1]; j++) {

            int neighbor = graph->edges[j];
            EdgeType bond = graph->weights[j];

            printf("%d(%f) ", neighbor, bond);
        }
        printf("\n");
    }
}
