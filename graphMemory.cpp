    /*

    Copyright (C) 2016 Md. Naim

    This file is part of Mesite - CUDA C++ parallel program to compute 1-matching. 

    Mesite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mesite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mesite.  If not, see <http://www.gnu.org/licenses/>.
    
    */

#include <stdio.h>

#include"graphMemory.h"
#include"stdlib.h"

static void releaseMemory(void* ptrToMemory) {
    if (ptrToMemory != NULL) {
        free(ptrToMemory);
        ptrToMemory = NULL;
    }
}

void freeGraph(Graph* graph) {
    if (graph) {

        releaseMemory(graph->indices);
        releaseMemory(graph->edges);

        if (graph->graphType == WEIGHTED)
            releaseMemory(graph->weights);

        releaseMemory(graph);

        printf("Memory released\n");
    }
}

Graph* allocateGraph(uint nrVertices, uint nrEdges, uint graphType) {


    Graph* graph = (Graph*) malloc(sizeof (Graph));

    graph->nrVertices = nrVertices;
    graph->nrEdges = nrEdges;
    graph->graphType = graphType;

    graph->indices = (uint*) malloc((nrVertices + 1) * sizeof (uint));
    graph->edges = (uint*) malloc(nrEdges * sizeof (uint));

    if (graphType == WEIGHTED) {
        graph->weights = (EdgeType*) malloc(nrEdges * sizeof (EdgeType));
    }

    return graph;

}

