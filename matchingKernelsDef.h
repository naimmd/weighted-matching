/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MATCHINGKERNELDEF_H
#define MATCHINGKERNELDEF_H

#include "graphdef.h"
#include "stddef.h"
#define min(X, Y) ((X) < (Y) ? (X) : (Y))

#ifdef RUNONGPU

__global__
#endif
    void
    kernel_OneMatchNonGeneric(Graph devGraph, uint *endPtsOfEdges,
                              uint *suitorIndices, uint *devCounters,
                              uint targetStep,
                              EdgeType *lowerBoundsOnSuitors /* = NULL */);

#ifdef RUNONGPU
__global__
#endif
    void
    kernel_OneMatching(Graph devGraph, uint *endPointsOfEdges,
                       uint *suitorIndices, uint *devCounters, uint targetStep);

#ifdef RUNONGPU
__global__
#endif
    void
    kernel_OneMatchBuffered(Graph devGraph, uint *endPointsOfEdges,
                            uint *suitorIndices, uint *devCounters,
                            uint targetStep,
                            EdgeType *lowerBoundsOnSuitors = NULL);

#ifdef RUNONGPU
__global__
#endif
    void
    kernelDecideEndPoints(Graph devGraph, uint *endPtsOfEdges);

// Utility Kernels
#ifdef RUNONGPU

__global__
#endif
    void
    kernel_Dump(Graph devGraph, uint *endPointsOfEdges, uint *suitorIndices,
                uint *devCounters, uint targetStep);

#ifdef RUNONGPU
__device__
#endif
    uint
    collectUnsucc(uint laneId, volatile uint *warpmem, uint nrVts2ReProcess,
                  uint isFinished, uint vId2Process);
#endif /* MATCHINGKERNELDEF_H */
