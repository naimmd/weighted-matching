/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "crossDevConstants.h"
#include "matchingKernelsDef.h"
#include "stdio.h"
#include <stdbool.h>

#define DWID 8
#define DEBUG 0

typedef struct {
  uint bestIdx;
  uint bestNbr;
} DataPair;

#ifdef RUNONGPU

__device__
#endif
    static void
    printGraphFromGPU(Graph *graph, uint *endPointsOfEdges,
                      uint *suitorIndices) {

  for (uint i = 0; i < min(graph->nrVertices, 18); i++) {
    printf("{v = %u: %u : |N(v)| %u}", i, suitorIndices[i],
           (graph->indices[i + 1] - graph->indices[i]));
    for (uint j = graph->indices[i]; j < graph->indices[i + 1]; j++) {
      printf("[%u %.2f]  ", graph->edges[j],
             graph->weights[j] /*, endPointsOfEdges[j]*/);
    }
    printf("\n");
  }
}

#ifdef RUNONGPU

__device__
#endif
    static DataPair
    findCandidate(uint vId, const Graph *graph, uint *endPtsOfEdges,
                  uint *suitorIndices, EdgeType *lowerBoundsOnSuitors = NULL) {

  uint startOfNbr = graph->indices[vId];
  uint endOfNbr = graph->indices[vId + 1];

  uint nrTE = graph->nrEdges;
  uint nrV = graph->nrVertices;

  uint bestIdx = nrTE;
  uint bestNbr = nrV;

  EdgeType bestWgt = -1.0;

  uint laneId = threadIdx.x & (WARPSIZE - 1); // NEVER USE wrpSz here!

  uint *edgesPtr = graph->edges;
  EdgeType *weightsPtr = graph->weights;

  for (int idx = startOfNbr + laneId; idx < endOfNbr; idx = idx + WARPSIZE) {

    uint y = edgesPtr[idx];

    if (y < nrV) { // if y wasn't modified to invalid

      EdgeType wgt2y = (weightsPtr != NULL) ? weightsPtr[idx] : 1.0;

      // compare against best found so far

      if (wgt2y < bestWgt)
        continue;
      if (wgt2y == bestWgt && y < bestNbr)
        continue; // prefer higherId

      /*
      //compare against current offer of neighbor
      uint partnerIdx = suitorIndices[y];
      if (partnerIdx < nrTE) {
          uint currPartner = endPtsOfEdges[partnerIdx];
      //graph->edges[partnerIdx]; //NOTE IT EdgeType wgtFromPartner =
      (weightsPtr != NULL) ? weightsPtr[partnerIdx] : 1.0;

          if (wgt2y < wgtFromPartner) {
              edgesPtr[idx] = NOSUITOR;
              continue;
          }

          if (wgt2y == wgtFromPartner && vId < currPartner) {
              edgesPtr[idx] = NOSUITOR;
              continue;
          }
      }
      */

      if (wgt2y < lowerBoundsOnSuitors[y]) {
        edgesPtr[idx] = NOSUITOR;
        continue;
      }

      // Save the best
      bestWgt = wgt2y;
      bestNbr = y;
      bestIdx = idx;
    }
  }

  for (int i = WARPSIZE >> 1; i >= 1; i = i >> 1) {

    EdgeType recvWgt = __shfl_xor_sync(bestWgt, i, WARPSIZE); // float
    uint recvVtx = __shfl_xor_sync(bestNbr, i, WARPSIZE);     // integer
    uint recvIdx = __shfl_xor_sync(bestIdx, i, WARPSIZE);     // integer

    if (recvWgt > bestWgt) {
      bestWgt = recvWgt;
      bestNbr = recvVtx;
      bestIdx = recvIdx;

    } else if (recvWgt == bestWgt) { // give priority to higher indexed one
      if (recvVtx > bestNbr) {
        bestWgt = recvWgt;
        bestNbr = recvVtx;
        bestIdx = recvIdx;
      }
    }
  }

  // Only thread zero of the warp try to grab and mark
  if (!laneId)
    edgesPtr[bestIdx] = NOSUITOR;

  // After reduction, everyone has same bestIdx
  DataPair rtnPair;
  rtnPair.bestIdx = bestIdx;
  rtnPair.bestNbr = bestNbr;

  if (DEBUG && !laneId)
    printf("\nvId=%u choose %u by %f\n", vId, bestNbr, bestWgt);

  return rtnPair;
}

#ifdef RUNONGPU

__device__
#endif
    static uint
    beSuitor_2(Graph *graph, uint *endPointsOfEdges, uint *suitorIndices,
               DataPair myPair, EdgeType *lowerBoundsOnSuitors = NULL) {

  uint rtnVtx = graph->nrVertices;
  uint idxOfBest = myPair.bestIdx;

  if (idxOfBest < graph->nrEdges) {

    uint vId = endPointsOfEdges[idxOfBest];
    uint bestNbr = myPair.bestNbr;

    if (bestNbr <
        graph->nrVertices) { // Managed to grab the edge without Fight (b=1)

      EdgeType *weightsPtr = graph->weights;
      EdgeType bestWgt = (weightsPtr != NULL) ? weightsPtr[idxOfBest] : 1.0;
      bool done = false;

      while (!done) {

        uint partnerIdx = suitorIndices[bestNbr];

        if (partnerIdx == NOSUITOR) { // NULL SUITOR, grab it

          uint resultOfAtomic =
              atomicCAS(&suitorIndices[bestNbr], NOSUITOR, idxOfBest);

          if (NOSUITOR == resultOfAtomic) {

            if (DEBUG)
              printf("\n%u became suitor of %u ----------------\n", vId,
                     bestNbr);

            lowerBoundsOnSuitors[bestNbr] = bestWgt; // update bound

            // graph->edges[idxOfBest] = NOSUITOR;
            done = true;
            break;
          }
          continue;
        }

        uint currSuitor = endPointsOfEdges[partnerIdx];

        EdgeType wgtFromCurrSuitor =
            (weightsPtr != NULL) ? weightsPtr[partnerIdx] : 1.0;

        if ((bestWgt > wgtFromCurrSuitor) ||
            (bestWgt == wgtFromCurrSuitor && vId > currSuitor)) {

          uint resultOfAtomic =
              atomicCAS(&suitorIndices[bestNbr], partnerIdx, idxOfBest);

          if (resultOfAtomic == partnerIdx) {

            lowerBoundsOnSuitors[bestNbr] = bestWgt;

            // graph->edges[idxOfBest] = NOSUITOR;

            rtnVtx = currSuitor;

            if (DEBUG)
              printf(
                  "\n%u became suitor of %u by replacing %u ----------------\n",
                  vId, bestNbr, rtnVtx);

            done = true;
            break;

          } // successful atomicCAS
          else {
            continue;
          }
        } // defeated current suitor
        else {
          rtnVtx = vId;
          done = true;
          break;
        }
      } // end of while(!done)
    }   // if(bestNbr < nrV)
  }     // if (idxOfBest < nrE)

  return rtnVtx;
}

#ifdef RUNONGPU

__device__
#endif
    static uint
    beSuitor(Graph *graph, uint *endPointsOfEdges, uint *suitorIndices,
             DataPair myPair, EdgeType *lowerBoundsOnSuitors = NULL) {

  uint nrTE = graph->nrEdges;
  uint nrTV = graph->nrVertices;

  uint returnVtx = nrTV;

  if (myPair.bestIdx < nrTE) { // Found a neighbor to offer

    uint bestNbr = graph->edges[myPair.bestIdx];

    // uint vId = endPointsOfEdges[myPair.bestIdx];

    uint prevSuitorIdx = myPair.bestNbr;

    uint currSuitorIdx =
        atomicCAS(&suitorIndices[bestNbr], prevSuitorIdx, myPair.bestIdx);

    if (currSuitorIdx == prevSuitorIdx) { // successfully offered
      lowerBoundsOnSuitors[bestNbr] = graph->weights[myPair.bestIdx];
      if (prevSuitorIdx < nrTE) { // dislodged a vertex

        returnVtx = endPointsOfEdges[prevSuitorIdx]; // Id of previous suitor
      } else {                                       // didn't dislodge anyone
        returnVtx = nrTV;                            // means, nothing to do
      }
    } else { // couldn't  offer; need to find new partner for vId
      returnVtx = endPointsOfEdges[myPair.bestIdx]; // vId;
    }
  } else {            // Didn't find a neighbor to offer; will remain unmatched
    returnVtx = nrTV; // means, nothing to do
  }
  return returnVtx;
}

#ifdef RUNONGPU

__device__
#endif
    static uint
    collectUnsucc_2(uint laneId, volatile uint *warpmem, uint nrVts2ReProcess,
                    uint isFinished, uint vId2Process) {

  uint value = 1 - isFinished;

  for (uint i = 1; i <= WARPSIZE / 2; i *= 2) {

    uint lowerLaneValue = __shfl_up_sync(value, i, WARPSIZE);
    if (laneId >= i)
      value += lowerLaneValue;
  }

  // Write in shared memory
  if (isFinished == 0) {
    warpmem[nrVts2ReProcess + (value - 1)] = vId2Process;
  }

  nrVts2ReProcess += __shfl_sync(value, WARPSIZE - 1, WARPSIZE);

  return nrVts2ReProcess;
}

#ifdef RUNONGPU

__global__
#endif
    void
    kernel_OneMatchNonGeneric(Graph devGraph, uint *endPtsOfEdges,
                              uint *suitorIndices, uint *devCounters,
                              uint targetStep,
                              EdgeType *lowerBoundsOnSuitors /* = NULL */) {

  // Global thread Id
  // uint threadId = blockIdx.x * blockDim.x + threadIdx.x;
  // local warp Id
  uint warpId = threadIdx.x / WARPSIZE;

  uint laneId = threadIdx.x & (WARPSIZE - 1);

  // Need to use local warpId
  extern __shared__ uint blockmem[];
  volatile uint *warpMem =
      blockmem + CHUNKSIZE * warpId; // each warp get memory of size (CHUNKSIZE)

  // global warp id
  warpId = blockIdx.x * (blockDim.x / WARPSIZE) + warpId;

  // count remaining vertices
  int nrVts2process = devGraph.nrVertices - warpId * CHUNKSIZE;

  if (nrVts2process >= CHUNKSIZE)
    nrVts2process = CHUNKSIZE;

  // if the warp got to do something
  if (nrVts2process > 0) {

    bool isFirstRound = true;
    // uint stepId = 0;
    uint nrVts2Reprocess = 0;

    DataPair myPair;
    myPair.bestIdx = devGraph.nrEdges; // Points to end of edge list
    // uint stp = 0;
    do {
      // if(++stp >6)break;
      uint offset = 0;
      uint vId;

      uint returnVtx = devGraph.nrVertices;

      for (offset = 0; offset < nrVts2process; offset++) {

        if (isFirstRound == true) {
          vId = warpId * CHUNKSIZE + offset;
        } else {
          vId = warpMem[offset];
        }

        DataPair rtnPair = findCandidate(vId, &devGraph, endPtsOfEdges,
                                         suitorIndices, lowerBoundsOnSuitors);
        // findCandidate(vId, &devGraph, endPtsOfEdges, suitorIndices, &bestIdx,
        // &prevSuitorIdx);

        if (offset % WARPSIZE == laneId) { // memorize it in register
          myPair = rtnPair;
        }
        if ((offset + 1) % WARPSIZE == 0) {

          returnVtx = beSuitor_2(&devGraph, endPtsOfEdges, suitorIndices,
                                 myPair, lowerBoundsOnSuitors);
          nrVts2Reprocess =
              collectUnsucc_2(laneId, warpMem, nrVts2Reprocess,
                              !(returnVtx < devGraph.nrVertices), returnVtx);
        }
      }

      returnVtx = devGraph.nrVertices;
      if (offset % WARPSIZE) {

        if (laneId < (offset % WARPSIZE)) {
          returnVtx = beSuitor_2(&devGraph, endPtsOfEdges, suitorIndices,
                                 myPair, lowerBoundsOnSuitors);
        }

        // NOTE: Whole warp needs to call this
        nrVts2Reprocess =
            collectUnsucc_2(laneId, warpMem, nrVts2Reprocess,
                            !(returnVtx < devGraph.nrVertices), returnVtx);
      }
      nrVts2process = nrVts2Reprocess;
      nrVts2Reprocess = 0;
      isFirstRound = false;

    } while (nrVts2process > 0);
  }
}
