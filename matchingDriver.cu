/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdbool.h>
#include <stdlib.h>

#include "assert.h"
#include "crossDevConstants.h"
#include "graphMemoryDev.h"
#include "matchChecker.h"
#include "matchingDriver.h"
#include "matchingKernelsDef.h"
#include "stddef.h"
#include "stdio.h"

void printTasks(uint *tasks, size_t nrTasks) {
  if (nrTasks > 40)
    return;
  for (size_t i = 0; i < nrTasks; i++) {
    printf("%u ", tasks[i]);
    if ((i + 1) % bValue == 0)
      printf("\n");
  }
}

void printBounds(EdgeType *bounds, size_t nrV) {
  if (nrV > 40)
    return;
  for (size_t i = 0; i < nrV; i++) {
    printf("%f ", bounds[i]);
  }
  printf("\n");
}

void computeOneMatching(Graph *hostGraph, bool isOneMatch /* = true */) {

  if (bValue > 1) {
    printf("You are asking for %d-matching\n", bValue);
    printf("Sorry, This program only performs 1-matching\n");
    return;
  }
  // Allocate Graph on Device
  Graph *devGraph = NULL;
  devGraph = allocateGraphOnDev(hostGraph);

  // Allocate array for extra endpoints of edges
  uint *devEndPointsOfEdges = NULL;
  allocateDevMem((void **)&devEndPointsOfEdges,
                 hostGraph->nrEdges * sizeof(uint));

  assert(hostGraph->nrEdges == devGraph->nrEdges);

  // Allocate array for suitors of vertices
  uint *devSuitorIndices = NULL;

  uint szOfSuitorArray = hostGraph->nrVertices * sizeof(uint);

  if (!isOneMatch) {
    szOfSuitorArray = hostGraph->nrVertices * sizeof(uint) * bValue;
    printf("\nStarting b-matching with b=%d \n", bValue);
  }

  allocateDevMem((void **)&devSuitorIndices, szOfSuitorArray);
  // Set suitor of each woman to -1 (initially)
  int fillinValue = (1 << 8) - 1;
  assert(cudaSuccess ==
         cudaMemset(devSuitorIndices, fillinValue, szOfSuitorArray));

  // Copy graph from host to device
  copyGraphH2D(hostGraph, devGraph);

  // lets assign a thread per vertex
  unsigned int nrThreads = 128;
  unsigned int workPerBlk = CHUNKSIZE * (nrThreads / WARPSIZE);
  unsigned int nrBlocks = (devGraph->nrVertices + workPerBlk - 1) / workPerBlk;

  // Allocate array for debugging purpose
  uint *devCounters = NULL;
  size_t nrWarps = nrBlocks * (nrThreads / WARPSIZE);

  printf("\n#Block = %u  #Warp = %lu\n", nrBlocks, nrWarps);

  allocateDevMem((void **)&devCounters, nrWarps * sizeof(uint));

  fillinValue = 0;
  assert(cudaSuccess ==
         cudaMemset(devCounters, fillinValue, nrWarps * sizeof(uint)));

  printf("\n#Block = %u #Thread/Block= %u\n", nrBlocks, nrThreads);

  // Make sure both end points are available
  kernelDecideEndPoints<<<nrBlocks, nrThreads>>>(*devGraph,
                                                 devEndPointsOfEdges);

  size_t szOfSharedMem =
      (nrThreads / WARPSIZE) * CHUNKSIZE * sizeof(uint); // per block

  uint *devTasks = NULL;
  uint *hostTasks = NULL;
  EdgeType *devLowerBoundsOnSuitors = NULL;
  EdgeType *hostLowerBoundsOnSuitors = NULL;

  uint *devPtrsInBarray = NULL;

  if (!isOneMatch) {

    printf("\n Sorry, Only 1-matching \n");
  }

  if (isOneMatch) {

    allocateDevMem((void **)&devLowerBoundsOnSuitors,
                   hostGraph->nrVertices * sizeof(EdgeType));

    // lower bounds on suitors are set to zero(0.0)
    assert(cudaSuccess == cudaMemset(devLowerBoundsOnSuitors, 0,
                                     hostGraph->nrVertices * sizeof(EdgeType)));
  }

  // variable for debugging purpose
  uint targetStep = 1 << 30; // scanf("%u", &targetStep);

  assert(cudaSuccess == cudaDeviceSynchronize());

  cudaEvent_t start, stop;
  assert(cudaSuccess == cudaEventCreate(&start));
  assert(cudaSuccess == cudaEventCreate(&stop));

  assert(cudaSuccess == cudaEventRecord(start, 0));

  if (isOneMatch) {

    printf("\nCalling b=ONE; non-generic version..............\n");

    szOfSharedMem =
        (nrThreads / WARPSIZE) * CHUNKSIZE * sizeof(uint); // per block

    kernel_OneMatchNonGeneric<<<nrBlocks, nrThreads, szOfSharedMem>>>(
        *devGraph, devEndPointsOfEdges, devSuitorIndices, devCounters,
        targetStep, devLowerBoundsOnSuitors);
  } else {
    printf("\n Sorry, Only 1-matching \n");
  }

  assert(cudaSuccess == cudaEventRecord(stop, 0));
  assert(cudaSuccess == cudaEventSynchronize(stop));

  float elapsedTime = 0.0;
  assert(cudaSuccess == cudaEventElapsedTime(&elapsedTime, start, stop));
  printf("\nTime to find match %f milliseconds\n", elapsedTime);

  assert(cudaSuccess == cudaEventDestroy(start));
  assert(cudaSuccess == cudaEventDestroy(stop));

  assert(cudaSuccess == cudaDeviceSynchronize());

  // Bring data back to host
  uint *hostCounters = NULL;
  hostCounters = (uint *)calloc(nrWarps, sizeof(uint));
  copyArray(hostCounters, devCounters, nrWarps * sizeof(uint),
            cudaMemcpyDeviceToHost);

  uint *hostSuitorIndices = NULL;
  hostSuitorIndices = (uint *)malloc(szOfSuitorArray);
  copyArray(hostSuitorIndices, devSuitorIndices, szOfSuitorArray,
            cudaMemcpyDeviceToHost);

  uint *hostEndPtsOfEdges = NULL;
  hostEndPtsOfEdges = (uint *)malloc(hostGraph->nrEdges * sizeof(uint));
  copyArray(hostEndPtsOfEdges, devEndPointsOfEdges,
            hostGraph->nrEdges * sizeof(uint), cudaMemcpyDeviceToHost);

  if (isOneMatch == true || bValue == 1) { // if bValue =1 in b-matching
    checkOneMatching(hostGraph, hostEndPtsOfEdges, hostSuitorIndices);
    printMatch(hostGraph, hostEndPtsOfEdges, hostSuitorIndices);
  }
  // Go Green!
  freeDevMem(devSuitorIndices);
  free(hostSuitorIndices);

  freeDevMem(devEndPointsOfEdges);
  free(hostEndPtsOfEdges);

  freeDevGraph(devGraph);

  freeDevMem(devCounters);
  free(hostCounters);
  freeDevMem(devTasks);
  freeDevMem(devLowerBoundsOnSuitors);
  freeDevMem(devPtrsInBarray);
  if (hostTasks)
    free(hostTasks);
  if (hostLowerBoundsOnSuitors)
    free(hostLowerBoundsOnSuitors);
}
