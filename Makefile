#!/bin/bash
CUDAVERSION=12.1
CC = g++
CFLAGS = -I. -O3 -std=c++11
LIBS = 

DEPS = graphdef.h graphIO.h graphMemory.h crossDevConstants.h 
OBJS =  graphIO.o graphMemory.o graphCheck.o

DFLAGS= -D RUNONGPU 
HFLAG= -D DISPLAY_DETAILS
#Only for debugging
FLAGS_TO_HOST_COMPILER=-Xcompiler -rdynamic -lineinfo


CU_CC=/usr/local/cuda-$(CUDAVERSION)/bin/nvcc
CU_FLAGS = -I/usr/local/cuda-$(CUDAVERSION)/include -O3 -arch sm_80 --relocatable-device-code=true 
CU_LIBS = -L/usr/local/cuda-$(CUDAVERSION)/lib64 -lcudart -O3
CU_DEPS = graphdef.h graphIO.h graphCheck.h graphMemoryDev.h  graphMemory.h  matchChecker.h  matchingDriver.h  matchingKernelsDef.h crossDevConstants.h 
CU_OBJS = main.o graphIO.o graphMemory.o graphMemoryDev.o graphCheck.o matchingDriver.o  standaloneKernels.o matchChecker.o oneMatchNonGeneric.o oneMatchBuffered.o oneMatching.o


EXEC=run1Matching

all: $(EXEC)
	
%.o: %.cu $(CU_DEPS) crossDevConstants.h
	$(CU_CC) -o $@ -c $< $(CU_FLAGS) $(DFLAGS) 

%.o: %.cpp $(DEPS)
	$(CC) -o $@ -c $< $(CFLAGS)

$(EXEC): $(CU_OBJS)
	$(CU_CC) -o $@ $^ $(FLAGS_TO_HOST_COMPILER) $(LIBS) $(CU_LIBS)
clean:
	rm -f *.o $(EXEC)
