    /*

    Copyright (C) 2016 Md. Naim

    This file is part of Mesite - CUDA C++ parallel program to compute 1-matching. 

    Mesite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mesite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mesite.  If not, see <http://www.gnu.org/licenses/>.
    
    */
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include "matchChecker.h"
#include "crossDevConstants.h"

#define min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define NR_VTX_TO_DISPLAY 20

bool checkOneMatching(Graph* graph, uint* endPtsOfEdges, uint* suitorIndices) {

    uint nrPairs = 0;
    for (uint vId = 0; vId < graph->nrVertices; vId++) {

        uint ptr_vId = suitorIndices[vId]; // Suitor Index of vId

        if (ptr_vId < graph->nrEdges) {

            uint partner = endPtsOfEdges[ptr_vId];

            uint ptr_partner = suitorIndices[partner]; // Suitor Index of partner

            if (ptr_partner < graph->nrEdges) {

                if (vId != endPtsOfEdges[ptr_partner]) {
                    printf("ERROR1: Suitor of %u is  %u, but suitor of %u is %u\n",
                            vId, partner, partner, endPtsOfEdges[ptr_partner]);
                    return false;
                } else {
                    nrPairs++;
                    //if (nrPairs < NR_VTX_TO_DISPLAY)
                    //printf("\n%u %u\n", vId, partner);

                }
            } else {
                printf("ERROR2: Suitor of %u is %u, but %u has no suitor\n", vId, partner, partner);
                return false;
            }
        }
    }

    assert(nrPairs % 2 == 0);

    float quality = (double) nrPairs * 100.0 / (double) graph->nrVertices;
    printf("Valid Matching; Quality = %f\n", quality);
    return true;
}

void printMatch(Graph* graph, uint* endPtsOfEdges, uint* suitorIndices) {

    printf("\nDumping suitors for first  %d vertices:\n", NR_VTX_TO_DISPLAY);

    for (uint vId = 0; vId < min(graph->nrVertices, NR_VTX_TO_DISPLAY); vId++) {

        uint suitorIndex = suitorIndices[vId];
        if (suitorIndex < graph->nrEdges)
            printf("%u: %u [%u] \n", vId, endPtsOfEdges[suitorIndex], graph->edges[suitorIndex]);
        else
            printf("%u: - \n", vId);
    }
    printf("\n");
}
