/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRAPHMEMORYDEV_H
#define GRAPHMEMORYDEV_H

#include "graphdef.h"

Graph *allocateGraphOnDev(const Graph *hostGraph);

void copyGraphH2D(const Graph *hostGraph, Graph *devGraph);

void freeDevGraph(Graph *devGraph);

void allocateDevMem(void **memPtr, size_t nrBytes);
void copyArray(void *dest, const void *source, size_t nrBytes, uint direction);

void freeDevMem(void *memPtr);
// Graph* allocateGraph(uint nrVertices, uint nrEdges, uint graphType);
// void freeGraph(Graph* graph);

#endif /* GRAPHMEMORYDEV_H */
