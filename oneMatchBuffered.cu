/*

Copyright (C) 2016 Md. Naim

This file is part of Mesite - CUDA C++ parallel program to compute 1-matching.

Mesite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mesite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Mesite.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "crossDevConstants.h"
#include "matchingKernelsDef.h"
#include "stdio.h"
#include <stdbool.h>

#define DWID 8
#define DEBUG 0

typedef struct {
  uint bestIdx;
  uint prevSuitorIdx;
} DataPair;

#ifdef RUNONGPU

__device__
#endif
    static void
    printGraphFromGPU(Graph *graph, uint *endPointsOfEdges,
                      uint *suitorIndices) {

  for (uint i = 0; i < min(graph->nrVertices, 18); i++) {
    printf("{v = %u: %u : |N(v)| %u}", i, suitorIndices[i],
           (graph->indices[i + 1] - graph->indices[i]));
    for (uint j = graph->indices[i]; j < graph->indices[i + 1]; j++) {
      printf("[%u %.2f]  ", graph->edges[j],
             graph->weights[j] /*, endPointsOfEdges[j]*/);
    }
    printf("\n");
  }
}

#ifdef RUNONGPU

__device__
#endif
    static DataPair
    findCandidate(
        uint vId, const Graph *graph, uint *endPtsOfEdges,
        uint *suitorIndices /*, uint* bestIdxPtr, uint* prevSuitorIdxPtr*/,
        EdgeType *lowerBoundsOnSuitors = NULL) {
  // determine #neighbors
  uint startOfNbr = graph->indices[vId];
  uint endOfNbr = graph->indices[vId + 1];

  uint nrTE = graph->nrEdges;

  uint bestIdx = nrTE;
  uint prevSuitorIdx = nrTE;

  // uint bestIdx = *bestIdxPtr;
  // uint prevSuitorIdx = *prevSuitorIdxPtr;

  uint bestNbr = graph->nrVertices;
  EdgeType bestWgt = -1.0;

  // NOTE; ID 0 of original warp (according to WARPSIZE); not modified
  // one(according to wrpSz)

  uint laneId = threadIdx.x & (WARPSIZE - 1); // NEVER USE wrpSz here!

  uint *edgesPtr = graph->edges;
  EdgeType *weightsPtr = graph->weights;
  uint nrV = graph->nrVertices;

  for (int idx = startOfNbr + laneId; idx < endOfNbr; idx = idx + WARPSIZE) {
    uint y = edgesPtr[idx];
    if (y < nrV) { // if y wasn't modified to invalid
      EdgeType wgt2y = (weightsPtr != NULL) ? weightsPtr[idx] : 1.0;

      // printf("y=%u, wgt2y=%f\n", y, wgt2y);

      // compare against best found so far
      if (wgt2y < bestWgt)
        continue;
      if (wgt2y == bestWgt && y < bestNbr)
        continue; // prefer higherId

      // compare against current offer of neighbor
      uint partnerIdx = suitorIndices[y];
      /*
      if (partnerIdx < nrTE) {

          uint currPartner = endPtsOfEdges[partnerIdx];
      //graph->edges[partnerIdx]; //NOTE IT EdgeType wgtFromPartner =
      (weightsPtr != NULL) ? weightsPtr[partnerIdx] : 1.0;

          if (wgt2y < wgtFromPartner) {
              edgesPtr[idx] = NOSUITOR;
              continue;
          }
          if (wgt2y == wgtFromPartner && vId < currPartner) {
              edgesPtr[idx] = NOSUITOR;
              continue;
          }
      }
       */
      if (wgt2y < lowerBoundsOnSuitors[y]) {
        edgesPtr[idx] = NOSUITOR;
        continue;
      }
      // If the thread reach here ; that means current neighbor can be offered
      bestIdx = idx;
      bestNbr = y;
      bestWgt = wgt2y;
      prevSuitorIdx = partnerIdx;
    }
  }

  // Determine size of warp according to nrNbr to save time in reduction phase

  for (int i = WARPSIZE >> 1; i >= 1; i = i >> 1) {

    EdgeType recWgt = __shfl_xor_sync(bestWgt, i, WARPSIZE); // float
    uint recVtx = __shfl_xor_sync(bestNbr, i, WARPSIZE);     // integer
    uint recvIdx = __shfl_xor_sync(bestIdx, i, WARPSIZE);    // integer
    uint recvPrevSuitorIdx =
        __shfl_xor_sync(prevSuitorIdx, i, WARPSIZE); // integer

    if (recWgt > bestWgt) {
      bestNbr = recVtx;
      bestWgt = recWgt;
      bestIdx = recvIdx;
      prevSuitorIdx = recvPrevSuitorIdx;
    } else if (recWgt == bestWgt) { // give priority to higher indexed one
      if (recVtx > bestNbr) {
        bestNbr = recVtx;
        bestWgt = recWgt;
        bestIdx = recvIdx;
        prevSuitorIdx = recvPrevSuitorIdx;
      }
    }
  }
  DataPair rtnPair;
  rtnPair.bestIdx = bestIdx;
  rtnPair.prevSuitorIdx = prevSuitorIdx;
  if (!laneId)
    if (DEBUG)
      printf("\nvId=%u choose %u by %f bound[%u] = %f \n", vId, bestNbr,
             bestWgt, bestNbr, lowerBoundsOnSuitors[bestNbr]);

  if (rtnPair.bestIdx < graph->nrEdges) {
    uint savedNbr = graph->edges[rtnPair.bestIdx];
    if (graph->edges[rtnPair.bestIdx] >= graph->nrVertices)
      printf("\n WRONG %u savedNbr= %u\n", bestNbr, savedNbr);
  }
  if (rtnPair.bestIdx < graph->nrEdges) {
    if (endPtsOfEdges[rtnPair.bestIdx] >= graph->nrVertices)
      printf("\nHOW COME ?\n");

    uint start = graph->indices[vId];
    uint end = graph->indices[vId + 1];
    if (rtnPair.bestIdx >= start && rtnPair.bestIdx < end) {
    } else {
      printf("\nCAN'T HAPPEN\n");
    }
  }

  return rtnPair;
  //*bestIdxPtr = bestIdx;
  //*prevSuitorIdxPtr = prevSuitorIdx;
  // After reduction, everyone has same bestIdx
}

#ifdef RUNONGPU

__device__
#endif
    static uint
    beSuitor_2(Graph *graph, uint *endPointsOfEdges, uint *suitorIndices,
               DataPair myPair, EdgeType *lowerBoundsOnSuitors = NULL) {

  uint nrTE = graph->nrEdges;
  uint nrTV = graph->nrVertices;

  uint returnVtx = nrTV;

  if (myPair.bestIdx < nrTE) { // Found a neighbor to offer

    uint vId = endPointsOfEdges[myPair.bestIdx];

    uint bestNbr = graph->edges[myPair.bestIdx];

    if (bestNbr >= graph->nrVertices) {

      uint start = graph->indices[vId];
      uint end = graph->indices[vId + 1];

      if (myPair.bestIdx >= start && myPair.bestIdx < end) {

        printf("\nIN RANGE but modified\n");
      } else {
        printf("\nCAN'T HAPPEN 2\n");
      }

      printf("\nIMPOSSIBLE\n");
    }

    EdgeType bestWgt =
        (graph->weights != NULL) ? graph->weights[myPair.bestIdx] : 1.0;

    uint partnerIdx = suitorIndices[bestNbr];

    if (partnerIdx == NOSUITOR) { // NULL SUITOR, grab it

      uint resultOfAtomic =
          atomicCAS(&suitorIndices[bestNbr], NOSUITOR, myPair.bestIdx);

      if (NOSUITOR == resultOfAtomic) {

        if (DEBUG)
          printf("\n%u became suitor of %u ----------------\n", vId, bestNbr);

        lowerBoundsOnSuitors[bestNbr] = bestWgt; // update bound
        graph->edges[myPair.bestIdx] = NOSUITOR / 2;

      } else {
        returnVtx = vId; // SPECIAL return case for
      }

    } else { // NON-NULL SUITOR;

      uint currSuitor = endPointsOfEdges[partnerIdx];

      EdgeType wgtFromCurrSuitor =
          (graph->weights != NULL) ? graph->weights[partnerIdx] : 1.0;

      if ((bestWgt > wgtFromCurrSuitor) ||
          (bestWgt == wgtFromCurrSuitor && vId > currSuitor)) {

        uint resultOfAtomic =
            atomicCAS(&suitorIndices[bestNbr], partnerIdx, myPair.bestIdx);

        if (resultOfAtomic == partnerIdx) {

          lowerBoundsOnSuitors[bestNbr] = bestWgt;

          graph->edges[myPair.bestIdx] = NOSUITOR / 3;

          returnVtx = currSuitor;

          if (DEBUG)
            printf(
                "\n%u became suitor of %u by replacing %u ----------------\n",
                vId, bestNbr, returnVtx);

        } else {
          returnVtx = vId;
        }
      } else {

        graph->edges[myPair.bestIdx] = NOSUITOR - 1;

        returnVtx = vId; // work with vId again **
      }
    }
  }

  if (DEBUG)
    printf("\n--> return  %u\n", returnVtx);

  return returnVtx;
}

#ifdef RUNONGPU

__device__
#endif
    static uint
    beSuitor(Graph *graph, uint *endPointsOfEdges, uint *suitorIndices,
             DataPair myPair, EdgeType *lowerBoundsOnSuitors = NULL) {

  uint nrTE = graph->nrEdges;
  uint nrTV = graph->nrVertices;

  uint returnVtx = nrTV;

  if (myPair.bestIdx < nrTE) { // Found a neighbor to offer

    uint bestNbr = graph->edges[myPair.bestIdx];

    // uint vId = endPointsOfEdges[myPair.bestIdx];

    uint prevSuitorIdx = myPair.prevSuitorIdx;

    uint currSuitorIdx =
        atomicCAS(&suitorIndices[bestNbr], prevSuitorIdx, myPair.bestIdx);

    if (currSuitorIdx == prevSuitorIdx) { // successfully offered
      lowerBoundsOnSuitors[bestNbr] = graph->weights[myPair.bestIdx];
      if (prevSuitorIdx < nrTE) { // dislodged a vertex

        returnVtx = endPointsOfEdges[prevSuitorIdx]; // Id of previous suitor
      } else {                                       // didn't dislodge anyone
        returnVtx = nrTV;                            // means, nothing to do
      }
    } else { // couldn't  offer; need to find new partner for vId
      returnVtx = endPointsOfEdges[myPair.bestIdx]; // vId;
    }
  } else {            // Didn't find a neighbor to offer; will remain unmatched
    returnVtx = nrTV; // means, nothing to do
  }
  return returnVtx;
}

#ifdef RUNONGPU

__device__
#endif
    uint
    collectUnsucc(uint laneId, volatile uint *warpmem, uint nrVts2ReProcess,
                  uint isFinished, uint vId2Process) {

  uint value = 1 - isFinished;

  for (uint i = 1; i <= WARPSIZE / 2; i *= 2) {

    uint lowerLaneValue = __shfl_up_sync(value, i, WARPSIZE);
    if (laneId >= i)
      value += lowerLaneValue;
  }

  // Write in shared memory
  if (isFinished == 0) {
    warpmem[nrVts2ReProcess + (value - 1)] = vId2Process;
  }

  nrVts2ReProcess += __shfl_sync(value, WARPSIZE - 1, WARPSIZE);

  return nrVts2ReProcess;
}

#ifdef RUNONGPU

__global__
#endif
    void
    kernel_OneMatchBuffered(Graph devGraph, uint *endPtsOfEdges,
                            uint *suitorIndices, uint *devCounters,
                            uint targetStep,
                            EdgeType *lowerBoundsOnSuitors /* = NULL */) {

  // Global thread Id
  // uint threadId = blockIdx.x * blockDim.x + threadIdx.x;
  // local warp Id
  uint warpId = threadIdx.x / WARPSIZE;

  uint laneId = threadIdx.x & (WARPSIZE - 1);

  // Need to use local warpId
  extern __shared__ uint blockmem[];
  volatile uint *warpMem =
      blockmem + CHUNKSIZE * warpId; // each warp get memory of size (CHUNKSIZE)

  // global warp id
  warpId = blockIdx.x * (blockDim.x / WARPSIZE) + warpId;

  // count remaining vertices
  int nrVts2process = devGraph.nrVertices - warpId * CHUNKSIZE;

  if (nrVts2process >= CHUNKSIZE)
    nrVts2process = CHUNKSIZE;

  // if the warp got to do something
  if (nrVts2process > 0) {
    /*
    if (nrVts2process < CHUNKSIZE) {
        if (!laneId)printf("\n(oneMatchBuffered): threadId = %u, warpId = %u,
    #vts2process=%d\n", threadId, warpId, nrVts2process);
    }
     */

    bool isFirstRound = true;
    // uint stepId = 0;
    uint nrVts2Reprocess = 0;

    DataPair myPair;
    myPair.bestIdx = devGraph.nrEdges; // Points to end of edge list
    // uint stp = 0;
    do {
      // if(++stp >6)break;
      uint offset = 0;
      uint vId;

      // uint bestIdx = devGraph.nrEdges;
      // uint prevSuitorIdx = devGraph.nrEdges;
      uint returnVtx = devGraph.nrVertices;

      for (offset = 0; offset < nrVts2process; offset++) {

        if (isFirstRound == true) {
          vId = warpId * CHUNKSIZE + offset;
        } else {
          vId = warpMem[offset];
        }

        DataPair rtnPair = findCandidate(vId, &devGraph, endPtsOfEdges,
                                         suitorIndices, lowerBoundsOnSuitors);
        // findCandidate(vId, &devGraph, endPtsOfEdges, suitorIndices, &bestIdx,
        // &prevSuitorIdx);

        if (offset % WARPSIZE == laneId) { // memorize it in register
          myPair = rtnPair;
          // myPair.bestIdx = bestIdx;
          // myPair.prevSuitorIdx = prevSuitorIdx;
        }
        if ((offset + 1) % WARPSIZE == 0) {

          returnVtx = beSuitor_2(&devGraph, endPtsOfEdges, suitorIndices,
                                 myPair, lowerBoundsOnSuitors);
          nrVts2Reprocess =
              collectUnsucc(laneId, warpMem, nrVts2Reprocess,
                            !(returnVtx < devGraph.nrVertices), returnVtx);
        }
      }

      returnVtx = devGraph.nrVertices;

      if (offset % WARPSIZE) {
        // if(!laneId) printf("\nResiduals\n");
        if (laneId < (offset % WARPSIZE)) {
          returnVtx = beSuitor_2(&devGraph, endPtsOfEdges, suitorIndices,
                                 myPair, lowerBoundsOnSuitors);
        }

        // NOTE: Whole warp needs to call this
        nrVts2Reprocess =
            collectUnsucc(laneId, warpMem, nrVts2Reprocess,
                          !(returnVtx < devGraph.nrVertices), returnVtx);
      }
      nrVts2process = nrVts2Reprocess;
      nrVts2Reprocess = 0;
      isFirstRound = false;

    } while (nrVts2process > 0);
  }
}
